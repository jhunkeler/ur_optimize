# Why do I want to use ur_optimize? #

The version of ATLAS bundled with Ureka (http://ssb.stsci.edu/ureka) is not designed to work on a large number of CPUs, therefore certain operations requiring linear algebra may perform inadequately to your needs. 

`ur_optimize` rebuilds the underlying ATLAS/LaPACK/BLAS stack to be tailored for your CPU's architecture. After that, the NumPy/SciPy stack is recompiled and linked against the new ATLAS implementation in order to take advantage of these optimizations.


# License #

BSD


# Requirements #

### RHEL 6.0+ / Fedora 10+ ###
```
sudo yum install gcc gfortran
```

### Ubuntu 12.04+ ###
```
sudo apt-get install build-essential
```


# Testing ur_optimize #

For this test I used the latest Ureka build (as of 09/04/2014):

Ureka_linux-rhe5_64_1.4.1.tar.gz

# Optimizing Ureka

```
$ git clone https://bitbucket.org/jhunkeler/ur_optimize.git
$ cd ur_optimize
$ ./ur_optimize -p /path/to/Ureka
```

Consider running `ur_optimize` in a `screen` session. ATLAS has been known to take a **very long time** to build.

### SYSTEM ###
```
Manufacturer: Dell Inc.
Product Name: OptiPlex 790
```

### BIOS ###

```
Vendor: Dell Inc.
Version: A07
Release Date: 09/10/2011
```


### CPU ###

```
vendor_id   : GenuineIntel
cpu family  : 6
model       : 42
model name  : Intel(R) Core(TM) i7-2600 CPU @ 3.40GHz
stepping    : 7
microcode   : 0x29
cpu MHz     : 3399.867
cache size  : 8192 KB
physical id : 0
siblings    : 8
core id     : 3
cpu cores   : 4
```


### MEMORY INFORMATION ###
```
size            : 16GB
type            : DDR3
Mhz             : 1333
```


## test_linalg.py module ##
```
#!/usr/bin/env python
import numpy
import scipy.interpolate

def interpolate(user_methods=None, points=1000):
    x = numpy.linspace(1.0, 1000.0, num=points)
    y = numpy.linspace(1.0, 1000.0, num=points)

    methods = ['linear', 'nearest', 'zero', 'slinear', 'quadratic', 'cubic']
    if user_methods is not None:
        methods = user_methods

    for method in methods:
        print("{}...".format(method)),
        try:
            scipy.interpolate.interp1d(x, y, kind=method, bounds_error=False, fill_value=0.0)
        except MemoryError:
            # Common ocurrance with quadratic
            print("failed")
        
        print("ok")

if __name__ == "__main__":
    interpolate()
```


## Performance Comparison ##


### Before ur_optimize ###
```
$ ipython
Python 2.7.5 (default, Jun 19 2014, 11:22:38) 
Type "copyright", "credits" or "license" for more information.

IPython 2.0.0 -- An enhanced Interactive Python.
?         -> Introduction and overview of IPython's features.
%quickref -> Quick reference.
help      -> Python's own help system.
object?   -> Details about 'object', use 'object??' for extra details.

In [1]: import test_linalg

In [2]: %timeit test_linalg.interpolate(['quadratic'])
quadratic... ok
quadratic... ok
quadratic... ok
quadratic... ok
1 loops, best of 3: 39 s per loop
```

**39** seconds per operation.


### After ur_optimize ###

```
$ ipython
Python 2.7.5 (default, Jun 19 2014, 11:22:38) 
Type "copyright", "credits" or "license" for more information.

IPython 2.0.0 -- An enhanced Interactive Python.
?         -> Introduction and overview of IPython's features.
%quickref -> Quick reference.
help      -> Python's own help system.
object?   -> Details about 'object', use 'object??' for extra details.

In [1]: import test_linalg

In [2]: %timeit test_linalg.interpolate(['quadratic'])
quadratic... ok
quadratic... ok
quadratic... ok
quadratic... ok
1 loops, best of 3: 1.17 s per loop
```

**1.17** seconds per operation.

# Found a bug? #

If ATLAS fails to compile for your architecture, please contact the developers of ATLAS, not me: http://math-atlas.sourceforge.net/faq.html#help

Otherwise, feel free to submit a bug report on this project's issue tracker: http://bitbucket.org/jhunkeler/ur_optimize/issues